+++
title = 'Wagner Moura'
date = 2023-09-25T17:59:36-03:00
draft = false
+++

"Agora o bicho vai pegar". Wagner Moura é um renomado ator baiano e diretor de cinema. Ele ganhou reconhecimento internacional por sua atuação como Pablo Escobar na série "Narcos". Além de seu sucesso em Hollywood, Moura é um defensor de causas sociais e políticas, usando sua fama para promover mudanças positivas no Brasil.

![wagner](https://aventurasnahistoria.uol.com.br/media/uploads/brasil/tropa_de_elite.jpg "Wagner")