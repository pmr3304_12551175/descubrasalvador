+++
title = 'Pitty'
date = 2023-09-25T17:59:36-03:00
draft = false
+++

"Que você me adora / Que me acha fod%". Pitty é uma das baianas mais influentes cantoras e compositoras de rock do Brasil. Com sua voz marcante e letras impactantes, ela se destaca como uma das vozes femininas mais proeminentes do rock nacional. Pitty é conhecida por sua atitude rebelde e comprometimento com a autenticidade.

![pitty](https://pbs.twimg.com/profile_images/1630934497926475777/HBPDJqq7_400x400.jpg "Pitty")