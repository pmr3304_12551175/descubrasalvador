+++
title = 'Ivete Sangalo'
date = 2023-09-25T17:59:36-03:00
draft = false
+++

Ivete Sangalo é uma das cantoras mais amadas do Brasil e da Bahia e uma das maiores estrelas do gênero musical conhecido como axé. Sua energia contagiante no palco e sua voz poderosa a tornaram um ícone da música brasileira. Além de sua carreira musical de sucesso, Ivete é conhecida por seu carisma e envolvimento em causas sociais.

![ivete](https://conteudo.imguol.com.br/c/splash/e7/2021/12/17/ivete-sangalo-1639760257904_v2_900x506.jpg.webp "Ivete Sangalo")