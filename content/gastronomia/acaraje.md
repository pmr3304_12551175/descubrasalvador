+++
title = 'Acarajé'
date = 2023-09-25T17:59:36-03:00
draft = false
+++

A Bahia é um dos estados brasileiros onde podemos encontrar as melhores iguarias, exemplo disso é o acarajé! Esta especialidade com influências africanas trata-se de uma espécie de sanduíche preparado com massa de feijão-fradinho e recheado com camarão e vatapá. Nas principais praias de Salvador, é servido com pimenta, camarão seco, caruru e vatapá (também uma iguaria africana), sendo relativamente comum a adição de vinagrete.

Custo médio: R$5 ~ R$20

![acaraje](https://receitinhas.com.br/wp-content/uploads/2017/01/4041c8a2f0a118ee65ce17d0b996c6cd-730x365.jpg "Acarajé")