+++
title = 'Cocada'
date = 2023-09-25T17:59:36-03:00
draft = false
+++

A cocada é um dos doces baianos mais tradicionais, seja cremosa, de corte, de forno, branca, marrom ou qualquer outra variação, é difícil encontrar alguém que resista ao seu sabor marcante. Feita com ingredientes simples, como coco, açúcar e um toque de especiarias, a cocada baiana é um deleite doce e satisfatório que oferece uma explosão de sabores tropicais. É frequentemente encontrada em barracas de rua, mercados locais e confeitarias em Salvador.

Custo médio: R$3 ~ R$5

![cocada](https://receitatodahora.com.br/wp-content/uploads/2022/07/cocada-1.jpg.webp "Cocada")