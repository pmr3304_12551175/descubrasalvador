+++
title = 'Moqueca de dendê'
date = 2023-09-25T17:59:36-03:00
draft = false
+++

A moqueca tem sabor intenso e marcante, pois é preparada com ingredientes como leite de coco (de preferência natural), azeite de dendê, camarões secos e, em alguns casos, a pimenta se destaca no prato! Existem vários tipos de moqueca que são servidos nos restaurantes da Bahia, como a moqueca de peixe, de camarão, de polvo, de lagosta entre outros. É um prato que serve tranquilamente 3 a 4 pessoas. Entretanto, seu preço varia bastante, pois depende da disponibilidade do fruto de mar ou pescado.

Custo médio: R$80~R$200

![moqueca](https://cdn0.tudoreceitas.com/pt/posts/0/8/9/moqueca_de_peixe_baiana_9980_600.webp "Moqueca")