**Descubra Salvador**

No blog Descubra Salvador, nós o convidamos a mergulhar na rica cultura, história e gastronomia desta cidade deslumbrante. Descubra cada canto, cada prato e cada sorriso que fazem de Salvador um destino verdadeiramente inesquecível.

Não espere mais! Planeje sua viagem a Salvador hoje e deixe-se levar pelas maravilhas deste paraíso baiano. Afinal, Salvador é mais do que uma cidade; é uma experiência única.

![salvador](https://lh3.googleusercontent.com/p/AF1QipNyqjZ_BQdil0FBqgGfiDp-wtIgPKoKkZEJMMax=w1080-h608-p-no-v0 "salvador")