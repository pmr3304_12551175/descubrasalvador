+++
title = 'Farol da Barra'
date = 2023-09-25T17:59:36-03:00
draft = false
+++

O Farol da Barra é um ícone histórico que cumprimenta os visitantes à entrada da Baía de Todos os Santos. Com sua arquitetura imponente e localização deslumbrante, este farol foi construído no século XVII e é um dos mais antigos do continente americano. A vista panorâmica do oceano a partir do Farol da Barra é espetacular, especialmente durante o pôr do sol. Explore seu museu e mergulhe na fascinante história marítima da Bahia.

Funciona de terça a domingo, das 9h às 18h. Há gratuidade para menores de 7 anos e deficientes físicos.

Ingressos para o público geral:

Turista: R$ 15,00 (inteira).

Estudantes, professores e idosos: R$ 7,50.

Grupos escolares: R$ 6,50 por aluno.

Moradores: R$5,00.

![faroldabarra](https://tourb.com.br/img/lugares/salvador/farol-da-barra.jpg "Farol da Barra")

