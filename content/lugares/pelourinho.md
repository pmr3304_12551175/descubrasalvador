+++
title = 'Pelourinho'
date = 2023-09-25T17:59:36-03:00
draft = false
+++

O Pelourinho é um testemunho vivo do passado colonial de Salvador. Suas ruas de paralelepípedos, casarões coloridos e igrejas barrocas refletem a influência europeia que marcou a cidade desde o século XVI. Passear pelo Pelourinho é como fazer uma viagem no tempo, onde você pode sentir a atmosfera e a herança das culturas africana, indígena e portuguesa que se entrelaçaram aqui. Você encontrará músicos de rua talentosos tocando samba, axé e outros ritmos brasileiros, proporcionando uma trilha sonora envolvente enquanto você explora as vielas do bairro. Não deixe de visitar o Museu Afro-Brasileiro, que destaca a contribuição crucial da cultura africana para a identidade baiana. O Pelourinho abriga algumas das igrejas mais impressionantes de Salvador, incluindo a Igreja de São Francisco, uma obra-prima barroca adornada com ouro e detalhes esculpidos. 


![pelourinho](https://www.voltologo.net/wp-content/uploads/2022/12/pelourinho-salvador-bahia.jpg "Pelourinho")

