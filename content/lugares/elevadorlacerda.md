+++
title = 'Elevador Lacerda'
date = 2023-09-25T17:59:36-03:00
draft = false
+++

O Elevador Lacerda é um símbolo da engenhosidade arquitetônica e da conectividade em Salvador. Este elevador art déco liga a Cidade Alta à Cidade Baixa e oferece uma visão deslumbrante da baía e do Porto de Salvador. Suba até o topo e testemunhe a transformação do cenário urbano enquanto as cabines se movem suavemente. Este é um ponto de encontro cultural e um local popular para tirar fotos panorâmicas da cidade.

Funcionamento: Segunda a sexta das 7h às 22h

Ingresso: R$0,15 por viagem

![elevadorlacerda](https://cdn.culturagenial.com/imagens/visa-o-do-elevador-lacerda-cke.jpg "Elevador Lacerda")

